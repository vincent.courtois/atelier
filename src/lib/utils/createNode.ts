export function createNode(type: string, context: AudioContext) {
    switch(type) {
        case 'gain': return context.createGain();
        case 'oscillator': return context.createOscillator();
        case 'destination': return context.destination
    }
    return false;
}