import { AudioTrack } from "./AudioTrack";
import { NodeWrapper } from "./NodeWrapper";
import {getCurrentInstance} from 'vue'

export class MicrophoneRack extends AudioTrack {

    init(context: AudioContext) {
        this.context = context;
        this.destination = this.initializeDestination();

        this.nodes = []
        this.paused = false;
    }
    
    stop() {
        this.source.node.disconnect();
    }
}