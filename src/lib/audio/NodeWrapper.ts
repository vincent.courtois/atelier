/**
 * The node wrapper class is designed to delegate most of the calls to the
 * audio node whilst being able to add attributes to it as the name.
 */
export class NodeWrapper {
    node: AudioNode;
    name: string;
    type: string;
    constructor(node: AudioNode, type: string, name: string) {
        this.node = node;
        this.name = name;
        this.type = type;
    }
}