import { createNode } from '../utils/createNode';
import { NodeWrapper } from './NodeWrapper'

export class AudioTrack {
    context!: AudioContext;
    source!: NodeWrapper;
    nodes!: NodeWrapper[];
    destination!: NodeWrapper;
    paused!: boolean;

    init(context: AudioContext) {
        this.context = context;
        this.source = this.initializeOscillator();
        this.destination = this.initializeDestination();

        this.nodes = []
        this.paused = false;
        
        this.source.node.connect(this.destination.node)
    }

    refresh() {
        this.source.node.disconnect();
        if (this.nodes.length > 0) {
            if(!this.paused) {
              this.source.node.connect(this.nodes[0].node);
            }
              
            console.log(this.nodes[0].node)
            this.nodes[0].node.connect(this.destination.node);
            for (let i = 1; i < this.nodes.length - 1; ++i) {
                this.nodes[i - 1].node.disconnect();
                this.nodes[i - 1].node.connect(this.nodes[i].node);
            }
            this.nodes[this.nodes.length - 1].node.disconnect()
            this.nodes[this.nodes.length - 1].node.connect(this.destination.node);
        }
        else {
            this.source.node.connect(this.destination.node);
        }
        console.log(this.nodes.map(n => n.name))
    }

    initializeOscillator() {
        const oscillator: OscillatorNode = this.context.createOscillator()
        return new NodeWrapper(oscillator, "oscillator", "source")
    }

    start() {
        return (this.source.node as OscillatorNode).start();
    }

    initializeDestination() {
        console.log(this);
        return new NodeWrapper(this.context.destination, 'destination', 'output')
    }

    pause() {
        this.source.node.disconnect()
        this.paused = true
    }

    resume() {
        if (this.nodes.length > 0) {
            this.source.node.connect(this.nodes[0].node);
        }
        else {
            this.source.node.connect(this.destination.node);
        }
        this.paused = false
    }
    
    stop() {
        (this.source.node as OscillatorNode).stop();
    }
}
